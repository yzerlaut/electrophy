from neo.io import ElphyIO
import numpy as np
import sys
import json
import os

####### GET THE ANALOG SIGNALS FROM ELPHY FILES

def get_signals_continuous(filename):
    """.DAT file as input
    output = [t, signal1, signal2, ...]

    N.B : the ElphyIO module has been initially written for the
    episode mode the structure of the ElphyIO structure implies some
    constraints and does not make the DY quantities available. So we
    need to grab it manually from the Elphy datafiles. This can be
    done thanks to the Elphy script : 'generate_json_file.PG2', 
    see Elphy_code repository
    """
    File = str(filename)
    reader = ElphyIO(filename=File)
    seg = reader.read_block(lazy=False, cascade=True)
    sampling_freq = seg.segments[0].analogsignals[0].sampling_rate.magnitude.tolist()
    tstart = seg.segments[0].analogsignals[0].t_start.magnitude.tolist()
    tstop = seg.segments[0].analogsignals[0].t_stop.magnitude.tolist()

    t = np.arange(tstart, tstop, 1./sampling_freq)
    data = np.array(seg.segments[0].analogsignals, dtype='float32')

    try:
        params = get_metadata(filename)
        for ii in range(len(data)):
            dyi = 'DY'+str(ii+1)
            if params.has_key(dyi):
                factor = np.float(params[dyi])
            else:
                print '-----------------------------------------------------------'
                print 'no DY information available, putting default'
                print 'THIS SHOULD BE CHECKED, insure you have the right gain !!!'
                print '-----------------------------------------------------------'
                factor = 3.05185094759972E-0001
            data[ii] = data[ii]/factor
    except IOError:
        print "-------------------------------------------------------------------------"
        print "no metadata informations available for this file"
        print "you shoudl generate it with the generate_json_file.PG2 script"
        print "-------------------------------------------------------------------------"
        print "In continuous mode the range of values will be wrong !!!!!"
        for ii in range(len(data)):
            factor = 3.05185094759972E-0001
            data[ii] = data[ii]/factor

    return t, data


def get_signals_episode(filename):
    """
    .DAT file as input
    output = [t, episode[0], episode[1], ...]
    """
    File = str(filename)
    reader = ElphyIO(filename=File)
    seg = reader.read_block(lazy=False, cascade=True)

    sampling_freq = seg.segments[0].analogsignals[0].sampling_rate.magnitude.tolist()
    tstart = seg.segments[0].analogsignals[0].t_start.magnitude.tolist()
    tstop = seg.segments[0].analogsignals[0].t_stop.magnitude.tolist()

    params = {}
    try:
        params = get_metadata(filename)
    except IOError:
        print "-------------------------------------------------------------------------"
        print "no metadata informations available for this file"
        print "you shoudl generate it with the generate_json_file.PG2 script"
        print "-------------------------------------------------------------------------"
        print "In continuous mode the range of values will be wrong !!!!!"
        
    t = np.arange(tstart, tstop, 1./sampling_freq) # in ms this time !!
    DATA = []
    for i in range(len(seg.segments)):
        data = np.array(seg.segments[i].analogsignals, dtype='float32')
        for ii in range(len(data)):
            dyi = 'DY'+str(ii+1)
            if params.has_key(dyi):
                factor = np.float(params[dyi])
            else:
                factor = 3.05185094759972E-0001
            data[ii] = data[ii]/factor
        DATA.append(data)
    return t, np.array(DATA)

####### GET THE METADATA INFOS ASSOCIATED TO ELPHY FILES

def get_metadata(filename):
    """
    you give the DAT file as argument !!!!
    you'll get a dict()
    """
    if os.path.isfile(filename+'.json'):
        filename=filename+'.json'
    else:
        filename = filename.replace('1.DAT','.json')
    ## first we need to replace the backslashes in the file
    with open (filename, "r") as myfile:
        data=myfile.read().replace("\\","_") #\\"
    with open (filename, "w") as myfile:
        myfile.write(data)
    # then we can actually import the json file
    with file(filename) as fid:
        params = json.load(fid)
    return params

def get_experiment_name_and_time(filename):
    filename = filename.replace('1.DAT','')
    strings = filename.split('_')
    time = strings[0]+'.'+strings[1]+'.'+strings[2]
    exp = strings[3]
    for i in range(4,len(strings)):
        exp = exp+'_'+strings[i]
    return exp, time

